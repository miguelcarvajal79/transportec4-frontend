import { createRouter, createWebHistory } from 'vue-router'
import App from './App.vue'
import Home from './components/Home.vue'
import RutaCrear from './components/RutaCrear.vue'
import TipoVehiculoCrear from './components/TipoVehiculoCrear.vue'
import VehiculoCrear from './components/VehiculoCrear.vue'
import CiudadCrear from './components/CiudadCrear.vue'
import RutaConsultarModificar from './components/RutaConsultarModificar.vue'
import TipoVehiculoConsultarModificar from './components/TipoVehiculoConsultarModificar.vue'
import VehiculoConsultarModificar from './components/VehiculoConsultarModificar.vue'
import CiudadConsultarModificar from './components/CiudadConsultarModificar.vue'
import TarifaConsultarModificar from './components/TarifaConsultarModificar.vue'
import RutaRutas from './components/RutaRutas.vue'
import TipoVehiculos from './components/TipoVehiculos.vue'
import Vehiculos from './components/Vehiculos.vue'
import Ciudades from './components/Ciudades.vue'
import Tarifas from './components/Tarifas.vue'
import RolConsultar from './components/RolConsultar.vue'
import RolCrear from './components/RolCrear.vue'
import VentasCrear from './components/VentasCrear.vue'
import VentasConsultarModificar from './components/VentasConsultarModificar.vue'
import Ventas from './components/Ventas.vue'
import LogIn from './components/LogIn.vue'
import SignUp from './components/SignUp.vue'
import TarifaCrear from './components/TarifaCrear.vue'

const routes = [{
    path: '/',
    name: 'root',
    component: App
  },
  {
    path: '/usuario/logIn',
    name: "logIn",
    component: LogIn,
    meta: { requiresAuth: false }
  },
  {
    path: '/usuario/signUp',
    name: "signUp",
    component: SignUp,
    meta: { requiresAuth: false }
  },
  {
    path: '/home',
    name: "home",
    component: Home
  },
  {
    path: '/RutaCrear',
    name: "rutacrear",
    component: RutaCrear
  },
  {
    path: '/TipoVehiculoCrear',
    name: "tipovehiculocrear",
    component: TipoVehiculoCrear
  },  
  {
    path: '/VehiculoCrear',
    name: "vehiculocrear",
    component: VehiculoCrear
  },
  {
    path: '/CiudadCrear',
    name: "ciudadcrear",
    component: CiudadCrear
  },
  {
    path: '/RutaConsultarModificar',
    name: "rutaconsultarmodificar",
    component: RutaConsultarModificar
  },
  {
    path: '/TipoVehiculoConsultarModificar',
    name: "tipovehiculoconsultarmodificar",
    component: TipoVehiculoConsultarModificar
  },
  {
    path: '/VehiculoConsultarModificar',
    name: "vehiculoconsultarmodificar",
    component: VehiculoConsultarModificar
  },
  {
    path: '/CiudadConsultarModificar',
    name: "ciudadconsultarmodificar",
    component: CiudadConsultarModificar
  },    
  {
    path: '/TarifaConsultarModificar',
    name: "tarifaconsultarmodificar",
    component: TarifaConsultarModificar
  },
  {
    path: '/RutaRutas',
    name: "rutarutas",
    component: RutaRutas
  },  
  {
    path: '/TipoVehiculos',
    name: "tipovehiculos",
    component: TipoVehiculos
  },
  {
    path: '/Vehiculos',
    name: "vehiculos",
    component: Vehiculos
  },
  {
    path: '/Ciudades',
    name: "ciudades",
    component: Ciudades
  },  
  {
    path: '/Tarifas',
    name: "tarifas",
    component: Tarifas
  },
  {
    path: '/RolConsultar',
    name: "RolConsultar",
    component: RolConsultar
  },
  {
    path:'/RolCrear',
    name:"RolCrear",
    component:RolCrear
  },
  {
    path:'/TarifaCrear',
    name:"tarifacrear",
    component:TarifaCrear
  },
  {
    path:'/VentasCrear',
    name:"ventascrear",
    component:VentasCrear
  },
  {
    path: '/VentasConsultarModificar',
    name: "ventasconsultarmodificar",
    component: VentasConsultarModificar
  },
  {
    path: '/Ventas',
    name: "ventas",
    component: Ventas
  }
];

const router = createRouter({
  history: createWebHistory(),
  routes
});

export default router;